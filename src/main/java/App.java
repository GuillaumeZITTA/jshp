import org.json.JSONException;
import shp.Drive;

public class App {
    public static void main(String[] args) throws JSONException {
        Drive drive = new Drive( System.getenv("GRAPH_USERNAME"), 
                                 System.getenv("GRAPH_PASSWORD"),
                                 System.getenv("GRAPH_CLIENTID"),
                                 System.getenv("GRAPH_SITEROOT"));
        //drive.GetDir("/").mkDir("test");
        //drive.GetDir("/test").upload("/home/neyta/mozilla.pdf", "mozilla.pdf");
        drive.ListFiles("/");
    }
}

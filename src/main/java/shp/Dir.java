package shp;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by neyta on 01/07/17.
 */
public class Dir implements Node {
    private final Drive drive;
    private final String path;




    public Dir(Drive drive, String path) {
        this.drive = drive;
        this.path = path;
    }

    public List<Node> List() {
        List<Node> retval = new ArrayList<>();

        try {
            JSONArray list = drive.GetList(path);

            for (int i = 0; i < list.length(); i++) {
                JSONObject entry = list.getJSONObject(i);
                String name = entry.getString("name");

                if (entry.has("folder")) {
                    retval.add(new Dir(drive, fullPath(name)));
                } else if (entry.has("file")) {
                    retval.add(new File(drive, fullPath(name)));
                }
            }
        } catch (Exception e) {
            System.err.println(e);
        }
        return retval;
    }

    public void mkDir(String name) throws JSONException {
        JSONObject folder = new JSONObject();
        folder.put("name", name);
        folder.put("folder", new JSONObject());
        System.out.println(folder.toString(4));
        System.out.println(drive.Post(path, folder));
    }

    public void upload(java.io.File local, String remote) {
        // drive.Put(remote)
    }

    public String fullPath(String name) {
        if (path.endsWith("/")) {
            return path + name;
        } else {
            return path + "/" + name;
        }
    }

    public boolean exists(String name) {
        return true;
    }



    @Override
    public String toString() {
        return "Dir{" +
                "path='" + path + '\'' +
                '}';
    }
}

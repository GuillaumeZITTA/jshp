package shp;

import com.microsoft.aad.adal4j.AuthenticationResult;
import org.json.*;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class Drive {

    private AuthenticationResult accessToken;
    private Request requester;

    public Drive(String username, String password, String clientId, String siteUrl) {
        requester = new Request(username, password, clientId, siteUrl);
    }


    public Response Get(String path) {
        return requester.Get("/drive/root/children" + pathExpend(path));
    }

    public Response Post(String path, JSONObject data) {
        return requester.Post("/drive/root/children" + pathExpend(path), data.toString());
    }

    public Response PutContent(String path, JSONObject data) {
        return requester.Get("/drive/root/children" + pathExpend(path));
    }
    public Response Put(String path, byte[] data, Map<String,String> headers) {
        return requester.Put("/drive/root/children" + pathExpend(path), data, headers);
    }




    public JSONArray GetList(String path) throws JSONException {
        Response r = requester.Get("/drive/root/children" + pathExpend(path));
        JSONObject obj = new JSONObject(r.getBody());
        return obj.getJSONArray("value");
    }

    public Dir GetDir(String path) {
        return new Dir(this, path);
    }

    public void ListFiles(String path) {
        System.out.println(GetDir(path).List());
    }

    public static String pathExpend(String path) {
        File f = new File(path);
        String retval = "";

        while (f.getName().length() > 0){
            retval = "/"+f.getName()+"/children"+retval;
            f = f.getParentFile();
        }
        return retval;
    }
}

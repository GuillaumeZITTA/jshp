package shp;

import org.json.JSONException;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by neyta on 02/07/17.
 */
public class File implements Node {
    private final Drive drive;
    private final String path;

    /**
     * Content Range header name.
     */
    private static final String CONTENT_RANGE_HEADER_NAME = "Content-Range";

    /**
     * Content Range value format.
     */
    private static final String CONTENT_RANGE_FORMAT = "bytes %1$d-%2$d/%3$d";

    /**
     * The seconds for retry delay.
     */
    private static final int RETRY_DELAY = 2 * 1000;

    /**
     * The max retry for single request.
     */
    private final int mMaxRetry = 5;

    /**
     * 10 Mo Chunksize
     */
    private static final int CHUNK_SIZE = 10 * 1024 * 1024;
    /**
     * The retry counter.
     */
    private int mRetryCount;

    public File(Drive drive, String path) {
        this.drive = drive;
        this.path = path;
    }

    /**
     * Upload a chunk with tries.
     *
     * @return The upload result.
     */
    public String uploadAChunk(final byte[] chunk,
                               final int totalLenth,
                               final int beginIndex) throws JSONException {


        Map<String, String> headers = new HashMap<>();
        headers.put(CONTENT_RANGE_HEADER_NAME,
                String.format(
                        CONTENT_RANGE_FORMAT,
                        beginIndex,
                        beginIndex + CHUNK_SIZE - 1,
                        totalLenth));

        while (this.mRetryCount < this.mMaxRetry) {
            try {
                Thread.sleep(RETRY_DELAY * this.mRetryCount * this.mRetryCount);
            } catch (final InterruptedException e) {
                System.err.println("Exception while waiting upload file retry: " + e);
            }

            Response result = null;

            try {
                result = drive.Put(path, chunk, headers);
            } catch (final Exception e) {
                System.err.println("Request failed with, retry if necessary: " + e);
            }

            if (result != null && result.getJSON().has("nextExpectedRanges")) {
                return result.getJSON().getString("nextExpectedRanges");
            }

            this.mRetryCount++;
        }

        throw new RuntimeException("Upload session failed to many times.");
    }

    @Override
    public String toString() {
        return "File{" +
                "path='" + path + '\'' +
                '}';
    }
}

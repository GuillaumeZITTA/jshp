package shp;

import com.microsoft.aad.adal4j.AuthenticationResult;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import java.util.Map;

/**
 * Created by neyta on 05/06/17.
 */
public class Request {
  
    private final String siteUrl;
   
    private AuthenticationResult accessToken;

    public Request(String username, String password, String clientId, String siteUrl) {
        this.siteUrl = siteUrl;
        try {
            this.accessToken = Auth.getAccessToken(username, password, clientId);
        } catch (Exception e) {
            System.out.println(e);
            System.exit(1);
        }
    }

    public AuthenticationResult getAccessToken() {
        return accessToken;
    }

    public Response Get(String path) {
        CloseableHttpClient httpclient = HttpClients.createDefault();
        try {
            HttpGet httpget = new HttpGet(siteUrl+path);
            httpget.setHeader("Authorization", "Bearer "+accessToken.getAccessToken());
            System.out.println("Executing request " + httpget.getRequestLine());
            return new Response(httpclient.execute(httpget));
        } catch (Exception e){
            throw new RuntimeException(e);
        }
    }

    public Response Post(String path, String data) {
        CloseableHttpClient httpclient = HttpClients.createDefault();
        try {
            HttpPost httppost = new HttpPost(siteUrl+path);
            httppost.addHeader("Authorization", "Bearer "+accessToken.getAccessToken());
            httppost.addHeader("Content-type", "application/json");
            StringEntity params =new StringEntity(data);
            httppost.setEntity(params);
            System.out.println("Executing request " + httppost.getRequestLine() + " " + httppost.getHeaders("Authorization")[0]);
            return new Response(httpclient.execute(httppost));
        } catch (Exception e){
            throw new RuntimeException(e);
        }
    }

    public Response Put(String path, String data, Map<String,String> headers) {
        CloseableHttpClient httpclient = HttpClients.createDefault();
        try {
            HttpPut httpput = new HttpPut(siteUrl+path);
            httpput.addHeader("Authorization", "Bearer "+accessToken.getAccessToken());
            httpput.addHeader("Content-type", "application/json");
            for (String key: headers.keySet()) {
                httpput.addHeader(key, headers.get(key));
            }
            StringEntity params =new StringEntity(data);
            httpput.setEntity(params);
            System.out.println("Executing request " + httpput.getRequestLine() + " " + httpput.getHeaders("Authorization")[0]);
            return new Response(httpclient.execute(httpput));
        } catch (Exception e){
            throw new RuntimeException(e);
        }
    }

    public Response Put(String path, byte[] data, Map<String,String> headers) {
        CloseableHttpClient httpclient = HttpClients.createDefault();
        try {
            HttpPut httpput = new HttpPut(siteUrl+path);
            httpput.addHeader("Authorization", "Bearer "+accessToken.getAccessToken());
            httpput.addHeader("Content-type", "application/octet-stream");
            for (String key: headers.keySet()) {
                httpput.addHeader(key, headers.get(key));
            }
            ByteArrayEntity params =new ByteArrayEntity(data);
            httpput.setEntity(params);
            System.out.println("Executing request " + httpput.getRequestLine() + " " + httpput.getHeaders("Authorization")[0]);
            return new Response(httpclient.execute(httpput));
        } catch (Exception e){
            throw new RuntimeException(e);
        }
    }
}

package shp;

import org.apache.http.Header;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.*;

/**
 * Created by neyta on 05/06/17.
 */
public class Response {
    private final Header contentType;
    private final StatusLine httpStatus;
    private String body = "";
    private int httpCode;
    private Map<String,String> headers;

    public Response(CloseableHttpResponse response) {
        this.httpStatus = response.getStatusLine();
        this.httpCode = this.httpStatus.getStatusCode();

        this.headers = new HashMap<>();
        this.setHeadersFromArray(response.getAllHeaders());
        this.contentType = response.getEntity().getContentType();
        try {
            this.body = EntityUtils.toString(response.getEntity());
            response.close();
        } catch (IOException e) {
            System.err.println(e);
        }
    }

    public void setHeadersFromArray(Header[] headers) {
        for (Header header: headers) {
            this.headers.put(header.getName(), header.getValue());
        }
    }

    public Header getContentType() {
        return contentType;
    }

    public String getBody() {
        return body;
    }

    public JSONObject getJSON() throws JSONException {
        return new JSONObject(getBody());
    }
    public int getHttpCode() {
        return httpCode;
    }

    public Map<String, String> getHeaders() {
        return headers;
    }

    @Override
    public String toString() {
        return "Response{" +
                "contentType=" + contentType +
                ", httpStatus=" + httpStatus +
                ", body='" + body + '\'' +
                ", headers=" + headers +
                '}';
    }
}

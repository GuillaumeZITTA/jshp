package shp;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Created by neyta on 01/07/17.
 */
public class DriveTest {
    @Test
    public void testPathExpend() {
        final String path = "/dir1/dir2";
        assertEquals("/dir1/children/dir2/children", Drive.pathExpend(path));
    }
}
